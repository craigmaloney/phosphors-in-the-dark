---
name: "next_steps"
title: "Next Steps"
reactions:
  - reaction: "Stick around and code some more."
    encounter: 'first_meeting'
    result: "Your brain gives you a few more line of code before it decides you've had enough. You head home to get some sleep."
    change:
      - name: 'you'
        passive_driven: 0.01
        war_fun: -0.01
  - reaction: "Go home and git some rest."
    encounter: 'first_meeting'
    result: "When you get home your brain is still thinking about the problem. You toss and turn until you finally pass out."
    change:
      - name: 'you'
        passive_driven: -0.01
        war_fun: 0.01
description: |
  You're not sure how, but Bob seems to know most of the folks in Silicon Valley. Where at first your team was just you and your best friend Arthur, now you have contacts with folks that can help with manufacturing, marketing, and even setting up a business plan. "A business plan?" you asked. "Yes", Bob replies. "Too many companies can't control their burn and flame out because they can't raise capital or they burn through capital too quickly. You need to have a plan and you need to stick to it. If you can't you're just going to be another name in a list of failed companies. We don't want that."
  
  If it weren't for Bob your company would have never even gotten off the ground. Unfortunately Bob doesn't have all of the answers.

  Arthur is your best friend. You and he have been through a lot of things together. That's why he was an obvious choice to bring into this whole ordeal. Unfortunately you quickly realized you're both in way over your head. Building a simple prototype was the easy part. The harder part is getting the machine to go from something that wouldn't excite the nerds at the Homebew Computer Club to something that's an actual, viable product.

  That's why you're working late yet again designing software for a machine that doesn't exist. Yet.

  Fortunately you were all able to cobble together a working development environment from some hand-me-down machines and some favors that Bob called in. You're not sure how Bob managed to swing what he did, but you can only assume that he knows exactly where the bodies are buried with some folks.

  You really hope that's figuratively and not literally.

  You're wondering if you should have sub-contracted the OS-level stuff with another company. Sure, you'll probably license a BASIC from some company out there, but the lower-level routines seem to be what you're good at programming. The main thing will be the disk-based operations. You hope that Felix has made some progress with that. Maybe you'll need to call a meeting in the morning.

  God, what time is it anyway? You shake your wrist to right your watch so you can read it by the glow of the monitor. It takes your eyes and brain a little while to register that it's 3:37 AM. Great. You quickly think if you're at a good stopping point with this code and cam pick it up when you're well-rested. That is, if you decide to go to sleep.

  "OK, self," you say aloud to nobody-in-particular, "what should I do: go to sleep or press on until daylight?"
