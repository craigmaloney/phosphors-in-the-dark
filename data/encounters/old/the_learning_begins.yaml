---
name: "the_learning_begins"
title: "The Learning Begins"
reactions:
  - reaction: "Get some sleep"
    encounter: 'the_epiphany'
    result: "You head to sleep still thinking about the problem."
description: |
  You settle on one book to get you started. You type in example programs into the machine and run them. Scraps of paper and notebooks now litter your kitchen table. The machine finally got a case, though you had to settle on a copier-paper box lid instead of the woodgrain case of your dreams. It's only temporary until you can get to making a proper case for it.

  You're amazed at how quickly mastered this arcane series of mnemonics and hexadecimal numbers. It's not hard, but he seems to have a knack for translating these various symbols into something the machine can understand. You wonder how long he's been working on this. It must have been a pretty long time.

  Your initial programs barely use the capabilities of the machine. Over time your code becomes more complex. Things that seemed daunting at the start become almost routine.

  You and Arthur have been to several Homebrew Computer Club meetings. Each time you come back with new ideas and new tricks to try with your fledgling machine. You hook up a cassette recorder to save your programs. Now cassettes join the papers and notebooks on the kitchen table. But the cassettes also allow folks to share programs with you that would otherwise be too long for you to type in. You also share little routines and code snippets with them. It's a symbiosis of learning how to make these machines work.

  Each meeting the master of ceremonies preaches his gospel of learning and empowerment. Concepts that seemed strange and alien to you are now becoming part of your being. Before you viewed computers as just simple tools for businesses to keep records and get information. Now you view them in a different light; a machine of endless possibilities. It's akin to Prometheus stealing fire from the gods.

  Too bad it requires a soldering iron and typing in long strings of characters into an unforgiving and temperamental demi-god named "monitor" to get said fire from said gods.

  Some folks at the club are working on porting the BASIC language to their various machines. With your current skills it seems like a pretty big task but perhaps when they're done you can take a look at the code and see how it works. That's the beauty if it all. These machines seem magical but the code is easily available to see how it works. Unlike magic the computer is more than happy to share its secrets, provided you know how to get there.

  But what of the others? The folks who don't understand how to get to the magic? Should everyone have to pay their dues in order to use this technology?

  You ponder this as you're entering more code when a quote attributed to Einstein pops into your brain: "Everything Should Be Made as Simple as Possible, But Not Simpler".

  But how to make this all simpler? You stare into space thinking for a while, but nothing immediately comes back to you.

  It's getting late. You decide to head to bed. 
