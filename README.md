# Phosphors in the Dark

**NOTE**: This project is complete. It is, however, _not_ finished. The story has a beginning but never progressed past that. I cover my reasoning in the following blog post: [When a plan doesn't come together](https://decafbad.net/2023/03/03/when-a-plan-doesn-t-come-together/). The Hearsay engine will be released separately, though, so hang tight for that release.

An interactive storyworld about the rise, fall, and end-days of the PC revolution.

The engine of this project is called "Hearsay", because it's based on the works of Chris Crawford's excellent "Storytron", "Wumpus" and "Le Morte D'Arthur" projects (as well as his essential books on Interactive Storytelling). It's called "Hearsay" because it's what I've heard / understood, and the telling of which might not be exact. If you want authenticity please see Chris' site "[Erasmatazz](https://erasmatazz.com)" for the source of truth.

All original artwork and text is released under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Attribution-ShareAlike 4.0 International License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a> license.
<br/>
<br/>
All code is licensed under the Affero GNU Public License v. 3.0. See LICENSE for more details.
